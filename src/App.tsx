import * as React from 'react';
import { Layout } from './shared/Layout';
import './main.global.css';
import { Header } from './shared/Header';
import { Content } from './shared/Content';
import { CardsList } from './shared/CardsList';

export function App(){
    return (
       <Layout>
           <Header/>
           <Content>
               <CardsList/>
           </Content>
       </Layout>
    )
} 
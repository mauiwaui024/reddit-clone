import React from 'react';
import styles from './card.less';
import { PreviewImg } from './PreviewImg';
import { TextContent } from './TextContent';
import { Menu } from './Menu';
import { Controls } from './Controls';
//Разделить карту на 4 элемента:
//1.Текстовый контент, куда входит вся метадата по юзеру и название карточки
//2.Превьюшка самой карточки
//3.Меню
//4.Кнопки управления
//Как сказала Ольга - нужно руководствоваться принципом "один компонент - одна ответственность". Это я и пытался сделать в общем-то, не знаю как еще описать алгоритм)

export function Card() {
  return (
    <li className={styles.card}>
      <TextContent/>
      <PreviewImg/>
      <Menu/>
      <Controls/>
    </li>
  );
}

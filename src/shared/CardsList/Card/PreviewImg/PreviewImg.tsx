import React from 'react';
import styles from './previewimg.less';

export function PreviewImg() {
  return (
    <div className={styles.preview}>
        <img className={styles.previewImg} src="https://cdn.dribbble.com/userupload/2817381/file/original-d365c1f88264a9ff1dedc91adcf32c96.png" alt="preview" />
      </div>
  );
}

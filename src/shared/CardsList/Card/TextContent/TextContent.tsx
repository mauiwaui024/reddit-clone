import React from 'react';
import styles from './textcontent.less';

export function TextContent() {
  return (
    <div className={styles.textContent}>
        <div className={styles.metaData}>
          <div className = {styles.userLink}>
            <img className={styles.avatar} src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Ed_Norton_Shankbone_Metropolitan_Opera_2009.jpg/1200px-Ed_Norton_Shankbone_Metropolitan_Opera_2009.jpg" alt="avatar" />
            <a href="#user-link" className = {styles.username}>Дмитрий Гришин</a>
          </div>
          <span className = {styles.createdAt}>
            <span className = {styles.publishedLabel}></span>
            4 часа назад</span>
        </div>
        <h2 className = {styles.title}>
          <a href="#post-url" className={styles.postLink}>
          Следует отметить, что новая модель организационной деятельности...
          </a>
        </h2>
      </div>
  );
}

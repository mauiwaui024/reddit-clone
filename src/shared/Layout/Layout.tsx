import React from 'react';
import styles from './layout.less';
//Чилдрены - это удобный способ один компонент в 
// другом без строгой привязки через импорты
interface ILayoutProps{
  children?: React.ReactNode
}

export function Layout({children}:ILayoutProps) {
  return (
    <div className={styles.layout}>
      {children}
    </div>
  );
}
